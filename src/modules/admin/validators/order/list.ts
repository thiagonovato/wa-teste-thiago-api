import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { PaginationValidator } from 'modules/common/validators/pagination';

export class ListValidator extends PaginationValidator {
  @IsString()
  @IsOptional()
  @ApiProperty({ required: false, enum: ['description', 'quantity', 'value', 'amount', 'createdDate', 'updatedDate'] })
  public orderBy: string;
}
