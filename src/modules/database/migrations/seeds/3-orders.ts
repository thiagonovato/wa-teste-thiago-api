import * as Knex from 'knex';
import { IOrder } from 'modules/database/interfaces/order';

export async function seed(knex: Knex): Promise<any> {
  const order: IOrder = {
    description: 'Primeiro registro',
    value: 25.5,
    quantity: 2,
    amount: 51,
    createdDate: new Date(),
    updatedDate: new Date()
  };

  await knex.insert(order).into('Order');
}
