export interface IOrder {
  id?: number;
  description: string;
  quantity: number;
  value: number;
  amount: number;

  createdDate?: Date;
  updatedDate?: Date;
}

